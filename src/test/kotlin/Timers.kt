import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.time.delay
import org.junit.Assert
import org.junit.Assert.assertTrue
import java.time.Duration
import java.time.LocalDateTime
import kotlin.math.absoluteValue

private const val ACCEPTED_PRECISION_SECONDS = 2

class Timers {

    lateinit var focusedProject: String
    var alreadyLoggedTime: Int = 0

    private val client = HttpClient()
    private val target = System.getenv("TARGET")

    private fun startTimer(projectName: String): Boolean = runBlocking {
        val result = client.put(target) {
            url {
                path("start")
            }
            contentType(ContentType.Application.Json)
            setBody(
                """
                {"name":"$projectName"}
            """.trimIndent()
            )
        }

        if (!result.status.isSuccess()) {
            Assert.assertEquals(403, result.status.value)
            //or 404, unexpected in functests for now
            return@runBlocking true
        }

        Assert.assertEquals(200, result.status.value)
        return@runBlocking true
    }

    private fun stopTimer(projectName: String): Boolean = runBlocking {
        val result = client.put(target) {
            url {
                path("stop")
            }
            contentType(ContentType.Application.Json)
            setBody(
                """
                {"name":"$projectName"}
            """.trimIndent()
            )
        }

        if (!result.status.isSuccess()) {
            result.bodyAsText()
            Assert.assertEquals(403, result.status.value)
            //or 404, unexpected in functests for now
            return@runBlocking true
        }

        Assert.assertEquals(200, result.status.value)
        return@runBlocking true
    }

    @Given("I will check the timer for the project {string}")
    fun givenFocusProject(name: String) {
        focusedProject = name
    }

    @Given("The timer is stopped")
    fun givenTimerIsStopped() {
        assertTrue(stopTimer(focusedProject))
    }

    @Given("I know the already logged time")
    fun givenMemorizeLoggedTime() {
        alreadyLoggedTime =
            Projects.retrieveProjects().stream()
                .filter() { project -> project.name == focusedProject }
                .findFirst()
                .get()
                .totalTime
    }

    @When("I start the timer")
    fun whenStartTimer() {
        startTimer(focusedProject)
    }

    @When("The lastTimer field correspond to now")
    fun whenLastTimerExpectedToNow() {
        val lastTimer =
            Projects.retrieveProjects().stream()
                .filter { project -> project.name == focusedProject }
                .findFirst()
                .get()
                .getLastTimerAsDate()
        assertMoreOrLessEqual(LocalDateTime.now().second, lastTimer!!.second, ACCEPTED_PRECISION_SECONDS)
    }

    @When("Wait for {int}sec")
    fun whenWait(seconds: Int) {
        runBlocking { delay(Duration.ofSeconds(10)) }
    }

    @Then("I expect to have a running timer at {int}sec")
    fun thenExpectRunningTime(seconds: Int) {
        val lastTimer =
            Projects.retrieveProjects().stream()
                .filter { project -> project.name == focusedProject }
                .findFirst()
                .get()
                .getLastTimerAsDate()
        assertMoreOrLessEqual(LocalDateTime.now().second, lastTimer!!.second + seconds, ACCEPTED_PRECISION_SECONDS)
    }

    @Then("I close the timer")
    fun thenCloseTimer() {
        assertTrue(stopTimer(focusedProject))
    }

    @Then("I expect to have {int}sec more logged to the project")
    fun thenExpectLoggedTime(seconds: Int) {
        val newTotal =
            Projects.retrieveProjects().stream()
                .filter { project -> project.name == focusedProject }
                .findFirst()
                .get()
                .totalTime
        val expectedNewTotal = alreadyLoggedTime + seconds
        assertMoreOrLessEqual(expectedNewTotal, newTotal, ACCEPTED_PRECISION_SECONDS)
    }

    private fun assertMoreOrLessEqual(val1: Int, val2: Int, precision: Int) {
        val diff = (val1 - val2).absoluteValue
        assertTrue(diff <= precision)
    }
}