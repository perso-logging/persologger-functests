import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Assert.assertTrue
import java.time.DayOfWeek
import java.time.LocalDateTime

class Analytics {

    lateinit var result: HttpResponse

    private val client = HttpClient() {
        install(ContentNegotiation) {
            json()
        }
    }
    private val target = System.getenv("TARGET")

    @When("I fetch the analytics")
    fun fetchAnalytics() = runBlocking {
        result = client.get(target) {
            url {
                path("analytics")
            }
        }

        Assert.assertEquals(200, result.status.value)
    }

    @Then("I expect to have 10sec logged for today on {string}")
    fun expect10sForToday(name: String) = runBlocking {
        val response: List<AnalyticsApi> = result.body()
        val analyticsApi = response.find { a -> a.projectName == name }

        assertTrue(analyticsApi!!.totalCurrentWeek == 1)
        assertTrue(analyticsApi.currentWeekDetails.getCurrentDay() == 1)
    }
}

private fun WeekDetails.getCurrentDay(): Int = when(LocalDateTime.now().dayOfWeek!!){
    DayOfWeek.MONDAY -> this.monday
    DayOfWeek.TUESDAY -> this.tuesday
    DayOfWeek.WEDNESDAY -> this.wednesday
    DayOfWeek.THURSDAY -> this.thursday
    DayOfWeek.FRIDAY -> this.friday
    DayOfWeek.SATURDAY -> this.saturday
    DayOfWeek.SUNDAY -> this.sunday
}
