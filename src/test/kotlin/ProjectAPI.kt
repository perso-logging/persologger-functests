import java.time.LocalDateTime

data class ProjectAPI(
    val name: String,
    val timerState: String,
    val lastTimer: String,
    val totalTime: Int
){
    fun getLastTimerAsDate(): LocalDateTime? {
        return try {
            LocalDateTime.parse(lastTimer)
        } catch (e: Exception) {
            null
        }
    }
}