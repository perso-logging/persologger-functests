import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JSR310Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.jsonMapper
import com.fasterxml.jackson.module.kotlin.kotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.junit.Assert.*

class Projects {

    companion object{
        enum class CREATION_STATUS { SUCCESS, ALREADY_EXIST }

        private val logger = KotlinLogging.logger {}
        private val client = HttpClient()

        private val target = System.getenv("TARGET")

    private val mapper = jacksonObjectMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        fun retrieveProjects() : List<ProjectAPI> = runBlocking {
            val result = client.request("$target/list")
            assertEquals(200, result.status.value)

            return@runBlocking mapper.readValue<List<ProjectAPI>>(result.bodyAsText())
        }
    }



    private fun createProject(projectName: String) : CREATION_STATUS = runBlocking {
        val result = client.post(target) {
            url {
                path("new")
            }
            contentType(ContentType.Application.Json)
            setBody("""
                {"name":"$projectName"}
            """.trimIndent())
        }

        if (!result.status.isSuccess()){
            assertEquals(403, result.status.value)
            return@runBlocking CREATION_STATUS.ALREADY_EXIST
        }

        assertEquals(201, result.status.value)
        return@runBlocking CREATION_STATUS.SUCCESS
    }

    private fun removeProject(projectName: String) : CREATION_STATUS = runBlocking {
        val result = client.delete("$target/remove") {
            url {
                path("remove")
                parameters.append("name", projectName)
            }
            contentType(ContentType.Application.Json)
            setBody("""
                {"name":"$projectName"}
            """.trimIndent())
        }

        if (!result.status.isSuccess()){
            assertEquals(404, result.status.value)
            return@runBlocking CREATION_STATUS.ALREADY_EXIST
        }

        assertEquals(200, result.status.value)
        return@runBlocking CREATION_STATUS.SUCCESS
    }

    @Given("There is no project")
    fun givenThereIsNoProject() {
        logger.info("thereIsNoProject()")
        val projects = retrieveProjects()

        for (project in projects) {
            removeProject(project.name)
        }

        assertEquals(0, retrieveProjects().size)
    }

    @When("I create a project named {string}")
    fun whenCreateProject(projectName: String) {
        createProject(projectName)
    }

    @Given("There is a project named {string}")
    fun givenProjectExist(projectName: String) {
        createProject(projectName)
    }

    @When("I delete a project named {string}")
    fun whenDeleteProject(projectName: String) {
        removeProject(projectName)
    }

    @Then("A project named {string} exists")
    fun thenProjectExist(projectName: String){
        assertTrue(retrieveProjects().stream().anyMatch { project -> project.name == projectName })
    }

    @Then("The list of project does not contains project {string}")
    fun thenProjectDoesNotExist(projectName: String) {
        assertFalse(retrieveProjects().stream().anyMatch { project -> project.name == projectName })
    }
}