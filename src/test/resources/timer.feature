Feature: Timers for logging time

  Scenario: Log 10sec of time on one project
    Given There is a project named "functest-timer"
    And I will check the timer for the project "functest-timer"
    And The timer is stopped
    And I know the already logged time
    When I start the timer
    And The lastTimer field correspond to now
    And Wait for 10sec
    Then I expect to have a running timer at 10sec
    And I close the timer
    And I expect to have 10sec more logged to the project
