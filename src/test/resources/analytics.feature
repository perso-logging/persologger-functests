Feature: User can have analytics about the time spent

  Scenario: User retrieve analytics for a project
    Given There is no project
    And I create a project named "functest-analytics"
    And I will check the timer for the project "functest-analytics"
    And I start the timer
    And Wait for 5sec
    And I close the timer
    When I fetch the analytics
    Then I expect to have 10sec logged for today on "functest-analytics"
