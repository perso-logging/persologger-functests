Feature: Projects are managed by user

  Scenario: Create one project
    Given There is no project
    When I create a project named "functest"
    Then A project named "functest" exists

  Scenario: Remove one project
    Given There is a project named "functest"
    When I delete a project named "functest"
    Then The list of project does not contains project "functest"
